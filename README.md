# SDR AlphaPage #

SDR AlphaPage combines SDR, GNURadio, rtl-sdr and multimonNG on Linux machines for real-time POCSAG data decoding and alphanumeric alert paging via e-mail, text message and the Google Hangouts application for local fire departments and enthusiasts.

## Initial Setup ##

On your device, download and install the rtl-sdr and multimonNG sources. rtl-sdr provides the drivers and narrow band FM demodulator needed for the USB dongle, while multimonNG will be utilized to decode the resulting POCSAG data from the receiver.

NOTE: If you leave the SDR plugged into your unit while installing rtl-sdr, you will need to unplug it and plug it back in for non-root access to work. It's best to leave it disconnected until installation is complete and if in doubt, simply reboot your machine when you're finished.

### Dependency Installation ###

The following has been tested on Debian 7.8 but should work on any distribution with some slight modifications.

    # Install dependencies
    sudo apt-get update
    sudo apt-get -y install build-essential  git cmake make gcc libusb-1.0 qt4-qmake libpulse-dev libx11-dev

    # Fetch and compile rtl-sdr source
    mkdir -p ~/src/
    cd ~/src/
    git clone git://git.osmocom.org/rtl-sdr.git
    cd rtl-sdr
    mkdir build
    cd build
    cmake ../ -DINSTALL_UDEV_RULES=ON
    make
    sudo make install
    sudo ldconfig

    # Fetch and compile multimonNG
    cd ~/src/
    git clone https://github.com/EliasOenal/multimonNG.git
    cd multimonNG
    mkdir build
    cd build
    qmake ../multimon-ng.pro
    make
    sudo make install


### To-Do List ###

*  "Initial" script (base)
*  User programmable frequency and mode
*  User programmable fire department alert
*  User programmable receive number/device/application
*  Implement "global settings", user can modify directly or through the software menu
*  Output to text file with "cut-off" at 200 alerts, simply erases oldest and stores newest page
*  Output to email/text server for sending to user
*  Output to Apache server for viewing via the web
*  Test and debug mode



### More info to be added ###